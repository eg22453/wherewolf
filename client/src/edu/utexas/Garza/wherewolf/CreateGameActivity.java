package edu.utexas.Garza.wherewolf;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class CreateGameActivity extends Activity {
	public void stopRegistering()
	{
		this.finish();
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_game);
		final Button button = (Button) findViewById(R.id.creategameButton);
		View.OnClickListener jim = new View.OnClickListener() {
			public void onClick(View v) {
				
				SharedPreferences sharedpreferences = getSharedPreferences("edu.utexas.werewolf.prefs", Context.MODE_PRIVATE);
				String username = sharedpreferences.getString("username", "");
				String password = sharedpreferences.getString("password", "");
				
			}
		};

		button.setOnClickListener(jim);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_game, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
