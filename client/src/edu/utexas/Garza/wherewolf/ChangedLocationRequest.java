package edu.utexas.Garza.wherewolf;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import edu.utexas.Garza.wherewolf.BasicRequest.RequestType;

public class ChangedLocationRequest extends BasicRequest{

	double latitude;
	double longitude;
	int gameId;

	public ChangedLocationRequest(String username, String password, double latitude, double longitude, int gameId) {

		super(username, password);

		this.latitude = latitude;
		this.longitude = longitude;
		this.gameId = gameId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getURL() {
		return "/v1/"+this.gameId+"update";
	}

	public List<NameValuePair> getParameters(){
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("username", username));
		urlParameters.add(new BasicNameValuePair("password", password));
		urlParameters.add(new BasicNameValuePair("latitude", Double.toString(latitude)));
		urlParameters.add(new BasicNameValuePair("longitude", Double.toString(longitude)));
		urlParameters.add(new BasicNameValuePair("gameId", Double.toString(gameId)));

		return urlParameters;
	}

	public RequestType getRequestType() {
		return RequestType.PUT;
	}

	public ChangedLocationResponse execute(WherewolfNetworking net){
		try {
			JSONObject response = net.sendRequest(this);

			if (response.getString("status").equals("success"))
			{
				// int playerID = response.getInt("playerid");
				return new ChangedLocationResponse("success", "signed in successfully");
			} else {

				String errorMessage = response.getString("error");
				return new ChangedLocationResponse("failure", errorMessage);
			}
		} catch (JSONException e) {
			return new ChangedLocationResponse("failure", "sign in not working");
		} catch (WherewolfNetworkException ex)
		{
			return new ChangedLocationResponse("failure", "could not communicate with the server");
		}



	}

}
