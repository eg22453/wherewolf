package edu.utexas.Garza.wherewolf;

public class Player {
	
	private int playerId;
	private String name;
	private String profilePicURL;
	private int numVotes;

	public Player(int playerID, String name, String profilePicURL, int numVotes){
		this.playerId = playerId;
		this.name = name;
		this.profilePicURL = profilePicURL;
		this.numVotes = numVotes;
		
	}

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		playerId = playerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfilePic() {
		return profilePicURL;
	}

	public void setProfilePicURL(String profilePicURL) {
		this.profilePicURL = profilePicURL;
	}

	public int getNumVotes() {
		return numVotes;
	}

	public void setNumVotes(int numVotes) {
		this.numVotes = numVotes;
	}

}
