package edu.utexas.Garza.wherewolf;

import java.util.List;

import org.apache.http.NameValuePair;

public class CreateGameRequest extends BasicRequest {

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public CreateGameRequest(String username, String password, 
			String name, String description) {

		super(username, password);

		this.name = name;
		this.description = description;

	}

	protected String name;
	protected String description;
	@Override
	public String getURL() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<NameValuePair> getParameters() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public RequestType getRequestType() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public BasicResponse execute(WherewolfNetworking net) {
		// TODO Auto-generated method stub
		return null;
	}

	// Basic getters and setters go here

}
