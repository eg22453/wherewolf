package edu.utexas.Garza.wherewolf;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import edu.utexas.Garza.wherewolf.WherewolfNetworking;

public class ChangeLocationRequest extends BasicRequest {

	private final double latitude;
	private final double longitude;
	private final int gameID;

	public ChangeLocationRequest(String username, String password, int gameID, double lat, double lng) {
		super(username, password);

		this.latitude = lat;
		this.longitude = lng;
		this.gameID = gameID;

	}

	@Override
	public String getURL() {
		return "/v1/game/" + this.gameID ;
	}

	@Override
	public List<NameValuePair> getParameters() {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("latitude", Double.toString(latitude)));
		params.add(new BasicNameValuePair("longitude", Double.toString(longitude)));
		return params;
	}

	@Override
	public RequestType getRequestType() {
		return RequestType.PUT;
	}

	@Override
	public ChangeLocationResponse execute(WherewolfNetworking net) {

		try {
			JSONObject jsonObject = net.sendRequest(this);

			// {"status:"success", current_time: 10020404053}

			if (jsonObject.getString("status").equals("success"))
			{
				String currentTime = jsonObject.getString("current_time");
				Long lCurTime = Long.parseLong(currentTime);
				return new ChangeLocationResponse("success", 
						"successfully updated the position", 
						lCurTime);

			}

			return new ChangeLocationResponse("failure", 
					jsonObject.getString("errorMessage") );
		} catch (JSONException ex)
		{
			return new ChangeLocationResponse("failure", 
					"could not parse the JSON" );
		} catch (WherewolfNetworkException ex)
		{
			return new ChangeLocationResponse("failure", 
					"could not communicate with server" );
		}

	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

}
