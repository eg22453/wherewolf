package edu.utexas.Garza.wherewolf;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class GameLobbyActivity extends Activity {
	
	private int startMainActivity()
	{
		//Log.v(TAG, "User pressed the register button");
		Intent intent = new Intent(this, MainScreenActivity.class);
		startActivity(intent);
		return 8;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_lobby);
		ArrayList<Player> arrayOfPlayers = new ArrayList<Player>();
		arrayOfPlayers.add(new Player(1, "Vegeta", "malevillager3", 5));
		arrayOfPlayers.add(new Player(2, "Marge", "malevillager3", 0));
		arrayOfPlayers.add(new Player(3, "Gimly", "malevillager3", 2));
		arrayOfPlayers.add(new Player(4, "Goku", "malevillager3", 3));
		// Create the adapter to convert the array to views
		PlayerAdapter adapter = new PlayerAdapter(this, arrayOfPlayers);
		// Attach the adapter to a ListView

		ListView playerListView = (ListView) findViewById(R.id.players_list);
		playerListView.setAdapter(adapter);
		
		final Button button = (Button) findViewById(R.id.startGameButton);// find the button in corresponding xml

		View.OnClickListener jim = new View.OnClickListener() {
			public void onClick(View v) {
				startMainActivity();
			}
		};
		
		button.setOnClickListener(jim);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_lobby, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
