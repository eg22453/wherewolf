package edu.utexas.Garza.wherewolf;

public class Game {
	
	private String gamename;
	private String gamedescription;
	private int gameID;
	private int status;
	private String adminName;
	
	
	public Game (String gamename, String gamedescription){
		this.gamename = gamename;
		this.gamedescription = gamedescription;
	}
	
	public String getGamename() {
		return gamename;
	}
	public void setGamename(String gamename) {
		this.gamename = gamename;
	}
	public String getGamedescription() {
		return gamedescription;
	}
	public void setGamedescription(String gamedescription) {
		this.gamedescription = gamedescription;
	}
	

}
