package edu.utexas.Garza.wherewolf;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class GameSelectionActivity extends Activity {

	private int startCreateGameActivity()
	{
		//Log.v(TAG, "User pressed the register button");
		Intent intent = new Intent(this,CreateGameActivity.class);
		startActivity(intent);
		return 8;
	}
	private int startLobbyActivity()
	{
		Intent intent = new Intent(this, GameLobbyActivity.class);
		startActivity(intent);
		return 8;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i("YCC", ">>>>> Creating game select");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_selection);

		final Button button = (Button) findViewById(R.id.creategameButton);
		// find the button in corresponding xml
		View.OnClickListener jam = new View.OnClickListener() {
			public void onClick(View v) {
				startCreateGameActivity();
			}
		};
		//
				button.setOnClickListener(jam);
		//
		ArrayList<Game> arrayOfGames = new ArrayList<Game>();
		arrayOfGames.add(new Game( "Game1", "Fight To the Death"));
		arrayOfGames.add(new Game( "Game2", "Fight To the Death"));
		arrayOfGames.add(new Game( "Game3", "Fight To the Death"));
		arrayOfGames.add(new Game( "Game4", "Fight To the Death"));
		//		arrayOfPlayers.add(new Player(2, "Marge", "malevillager3", 0));
		//		arrayOfPlayers.add(new Player(3, "Gimly", "malevillager3", 2));
		//		arrayOfPlayers.add(new Player(4, "Goku", "malevillager3", 3));
		//		// Create the adapter to convert the array to views
		GameAdapter adapter = new GameAdapter(this, arrayOfGames);
		//		// Attach the adapter to a ListView
		//
		ListView gameListView = (ListView) findViewById(R.id.games_list);
		gameListView.setAdapter(adapter);

		gameListView.setClickable(true);

		gameListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				startLobbyActivity();

			}




		});




	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_selection, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
