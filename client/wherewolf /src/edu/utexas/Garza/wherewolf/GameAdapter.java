package edu.utexas.Garza.wherewolf;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
public class GameAdapter extends ArrayAdapter<Game>{
	public GameAdapter(Context context, ArrayList<Game> games) {
		super(context, 0, games);
		
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Get the data item for this position
		Game game = getItem(position);    
		// Check if an existing view is being reused, otherwise inflate the view
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.game_selection, parent, false);
		}
		// Lookup view for data population
		//ImageView profileImg = (ImageView) convertView.findViewById(R.id.playerimg);
		TextView nameTV = (TextView) convertView.findViewById(R.id.game_name);
		TextView descripTV = (TextView) convertView.findViewById(R.id.game_description);
		// Populate the data into the template view using the data object
//		if (game.getProfilePic().equals("femalevillager1")){
//			// pull image for the female picture. etc
//		}
//		else
//		{
//			profileImg.setImageResource(R.drawable.villager3);
//		}
		nameTV.setText(game.getGamename());
		descripTV.setText(""+game.getGamedescription());
		// Return the completed view to render on screen
		return convertView;
	}
}
