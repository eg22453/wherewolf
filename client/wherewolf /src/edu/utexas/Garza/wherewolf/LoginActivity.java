package edu.utexas.Garza.wherewolf;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class LoginActivity extends Activity {

	private static final String TAG = "loginactivity";
	private int numberOfPresses;

	private int startRegisterActivity()
	{
		Log.v(TAG, "User pressed the register button");
		Intent intent = new Intent(this, RegisterActivity.class);
		startActivity(intent);
		return 8;
	}

	private int startGameSelectionActivity()
	{
		Log.v(TAG, "User pressed the login button");
		Intent intent = new Intent(this, GameSelectionActivity.class);
		startActivity(intent);
		return 9;
	}



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i("YCC", ">>>>> start on create");
		super.onCreate(savedInstanceState);
		//Log.i(TAG, "on create");
		setContentView(R.layout.activity_login);
		//Log.i(TAG, "created the login activity");

		final Button button = (Button) findViewById(R.id.registerButton);// find the button in corresponding xml

		View.OnClickListener jim = new View.OnClickListener() {
			public void onClick(View v) {
				startRegisterActivity();
			}
		};

		button.setOnClickListener(jim);
///////// now to set activity switch for login
		final Button loginbutton = (Button) findViewById(R.id.loginButton);// find the button in corresponding xml

		View.OnClickListener joy = new View.OnClickListener() {
			public void onClick(View v) {
				startGameSelectionActivity();// change this possibly along with all code above or delete
			}
		};

		loginbutton.setOnClickListener(joy);
		/*
		 * button.setOnClickListener(new View.OnClickListener() { public void
		 * onClick(View v) { // Perform action on click } });
		 */
	}






	@Override
	protected void onStart() {
		numberOfPresses = 0;
		Log.i(TAG, "started the login activity");
		super.onStart();
	}

	@Override
	protected void onRestart() {
		Log.i(TAG, "restarted the login activity");
		super.onRestart();
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "resumed the login activity");
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "pause the login activity");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.i(TAG, "stopped the login activity");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "destroyed the login activity");
		super.onDestroy();
	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the
	 * menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.login, menu); return true; }
	 * 
	 * @Override public boolean onOptionsItemSelected(MenuItem item) { // Handle
	 * action bar item clicks here. The action bar will // automatically handle
	 * clicks on the Home/Up button, so long // as you specify a parent activity
	 * in AndroidManifest.xml. int id = item.getItemId(); if (id ==
	 * R.id.action_settings) { return true; } return
	 * super.onOptionsItemSelected(item); }
	 */
}