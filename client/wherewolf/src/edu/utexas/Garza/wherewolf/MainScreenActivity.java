package edu.utexas.Garza.wherewolf;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class MainScreenActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_screen);
		ArrayList<Player> arrayOfPlayers = new ArrayList<Player>();
		arrayOfPlayers.add(new Player(1, "Vegeta", "malevillager3", 5));
		arrayOfPlayers.add(new Player(2, "Marge", "malevillager3", 0));
		arrayOfPlayers.add(new Player(3, "Gimly", "malevillager3", 2));
		arrayOfPlayers.add(new Player(4, "Goku", "malevillager3", 3));
		// Create the adapter to convert the array to views
		PlayerAdapter adapter = new PlayerAdapter(this, arrayOfPlayers);
		// Attach the adapter to a ListView

		ListView playerListView = (ListView) findViewById(R.id.players_list);
		playerListView.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_screen, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
